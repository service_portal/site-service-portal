include .env
include .creds
NAME := ds/$(DOCKER_NAME)
VERSION := $(or $(VERSION),$(VERSION),'latest')
PLATFORM := $(shell uname -s)
GET_DOCKER := $(shell [ -d docker ] || git clone $(DOCKER_REPO) docker)
include docker/Makefile